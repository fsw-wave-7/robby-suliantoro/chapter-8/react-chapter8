import React, {Component, Fragment} from 'react';
import Table from 'react-bootstrap/Table';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

class Homepage extends Component {
    
    constructor(props) {
        super(props);
    
                this.state = {
                    data: [
                        {
                            id:1,
                            username:"Agus",
                            email:"agus@gmail.com",
                            experience:2000,
                            level:2,
                        },
                        {
                            id:2,
                            username:"Budi",
                            email:"budi@gmail.com",
                            experience:1000,
                            level:1,
                        },
                    ],
                    id:null,
                    keyword:"",
                    username:"",
                    email:"",
                    experience:"",
                    level:"",
                    
                };

                // this.handleChange = this.handleChange.bind(this);
                // this.submitData = this.submitData.bind(this);
                // this.editData = this.editData.bind(this);
                this.find = this.find.bind(this);


        }

        handleChange = (key,value) => {
            console.log(key,value);

            // if (key == 'firstname') {
            //     this.setState({
            //         firstname:value
            //     })
            // } else if (key == 'lastname') {
            //     this.setState({
            //         lastname:value
            //     })
            // } else if (key == 'username') {
            //     this.setState({
            //         username:value
            //     })
            // } 

            this.setState({
                [key]:value,
            });
        };

        submitData = (event) => {
            event.preventDefault()
            const {data,  username, email, experience, level, id} = this.state // untuk ambil value dari state
            
            if (id) {
                //ambil yang dari form
                // const { data } = this.state
                // const playerNotChosen = data.filter((player) => { return player.id !== id})
                // playerNotChosen.push({username,email,experience,level,id})
                    
                    const playerNotChosen = data
                    .filter((player) => player.id !== id)
                    .map((playerFilter) => {
                        return playerFilter;
                    });

                this.setState({
                    //data:playerNotChosen
                    data: [
                        ...playerNotChosen,
                        {
                            id,
                            username,
                            email,
                            experience,
                            level
                        },
                    ],
                });
            }   else {
                    //data.push({username,email,experience,level, id:data.length+1})
                    this.setState ({  
                        //data
                        data : [
                            ...data,
                            {
                                id: data.length+1,
                                username,
                                email,
                                experience,
                                level,
                            }
                        ],
                    }); // update state
            }
        };
    
        editData = (index) => {
            // const {data} = this.state
            // const playerChosen = data.filter((player) => {return player.id ===index})
            
            const playerChosen = this.state.data
                .filter((player) => player.id === index )
                .map((playerFilter) => {
                    return playerFilter;
                });
            this.setState({
                //tampilin apa yang mau di edit
                id: playerChosen[0].id,
                username: playerChosen[0].username,
                email: playerChosen[0].email,
                experience: playerChosen[0].experience,
                level: playerChosen[0].level,
            });
        };

        find = () => {
            const { data, keyword } = this.state;
            const search = data.filter((hasilFilter) => {
                return (
                    hasilFilter.username.toLowerCase().includes(keyword.toLowerCase()),
                    hasilFilter.email.toLowerCase().includes(keyword.toLowerCase()),
                    hasilFilter.exp.includes(keyword),
                    hasilFilter.lvl.includes(keyword)
                )
            })
            console.log(search)
    
            this.setState({
                data: search
            })
        }
    
    
    render() {

        const {data,  username, email, experience, level, id, keyword} = this.state;
        console.log(data)

        return (
            <Fragment>
                <Container className="p-2">
                    <Row>

                    <Col xs={4}>
                            <h5>Form Penambahan / Edit Data</h5>
                            <Form onSubmit={this.submitData}>
                                
                                <Form.Group className="mb-3" controlId="id">
                                    <Form.Label>ID</Form.Label>
                                    <Form.Control 
                                        type="text" 
                                        placeholder="New User, ID dikosongi" 
                                        value={id}
                                        onChange={ (event) => this.handleChange('id', event.target.value)}
                                    />
                                </Form.Group>

                                <Form.Group className="mb-3" controlId="username">
                                    <Form.Label>Username</Form.Label>
                                    <Form.Control 
                                        type="text" 
                                        placeholder="Enter Username" 
                                        value={username}
                                        onChange={ (event) => this.handleChange('username', event.target.value)}
                                    />
                                </Form.Group>

                                <Form.Group className="mb-3" controlId="email">
                                    <Form.Label>Email</Form.Label>
                                    <Form.Control 
                                        type="text" 
                                        placeholder="Enter Email" 
                                        value={email}
                                        onChange={ (event) => this.handleChange('email', event.target.value)}
                                    />
                                </Form.Group>

                                <Form.Group className="mb-3" controlId="experience">
                                    <Form.Label>Experience</Form.Label>
                                    <Form.Control 
                                        type="text" 
                                        placeholder="Enter Experience Value" 
                                        value={experience}
                                        onChange={ (event) => this.handleChange('experience', event.target.value)}
                                    />
                                </Form.Group>

                                <Form.Group className="mb-3" controlId="level">
                                    <Form.Label>Level</Form.Label>
                                    <Form.Control 
                                        type="text" 
                                        placeholder="Enter Level Value" 
                                        value={level}
                                        onChange={ (event) => this.handleChange('level', event.target.value)}
                                    />
                                </Form.Group>
                                <Button variant="primary" type="submit">
                                    Submit
                                </Button>
                            </Form>
                        </Col>

                        <Col xs={8}> 
                            <Row>
                                <Col>
                                    <Form>
                                        <Col xs={6}>
                                            <Form.Group className="mb-3"controlId="search">
                                                <Form.Control type="text" placeholder="Masukkan keyword" 
                                                value={keyword}
                                                onChange={(event) => this.handleChange('keyword', event.target.value)}
                                                />
                                            </Form.Group>
                                        </Col>
                                        <Col xs={6}>
                                            <Button variant="primary" onClick={this.find}>Search</Button>
                                        </Col>
                                    </Form>       
                                </Col>
                                    
                            </Row>
                            <Row>
                            <h5>Ini adalah Tabel Data</h5>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                    <th><div style={{textAlign:"center"}}>No</div></th>
                                    <th><div style={{textAlign:"center"}}>Username</div></th>
                                    <th><div style={{textAlign:"center"}}>Email</div></th>
                                    <th><div style={{textAlign:"center"}}>Experience</div></th>
                                    <th><div style={{textAlign:"center"}}>Level</div></th>
                                    <th><div style={{textAlign:"center"}}>Action</div></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        data.map((player, index) => {
                                            return (
                                                <tr key={index}>
                                                    <td>{index+1}</td>
                                                    <td>@{player.username}</td>
                                                    <td>{player.email}</td>
                                                    <td>{player.experience}</td>
                                                    <td>{player.level}</td>
                                                    <td><Button variant="secondary" onClick={() => this.editData(player.id)}>Edit</Button></td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </Table>
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </Fragment>
        )
    }
}

export default Homepage;